build:
	dasm *.asm -f3 -v0 -ocart.bin

run: build
	stella -format NTSC cart.bin

run-pal: build
	stella -format PAL cart.bin

clean:
	rm -f cart.bin
