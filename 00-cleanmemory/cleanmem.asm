    processor 6502
    
    seg code
    org $F000       ; defines the code origin at $F000 (start of ROM memory)

Start:
    sei             ; disbale interrupt - all atari carts beginwith this
    cld             ; disable the BCD decimal math mode
    ldx #$FF        ; loads the X register with #$FF
    txs             ; Transfer x register to stack register

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Clear the Zero Page region (mearning memory addresses from $00 to $FF
; Meaning the entire TIA register space and also RAM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    lda #0          ; A = 0
    ldx #$00        ; X = #$00 (the first DEX below will roll us over to $FF and we will start erasing at FF, not 00

MemLoop:
    dex             ; X--
    sta $0,X        ; Store A register at address $0 + the value store in X
    bne MemLoop     ; Loop until x=0 (BNE = Branch if (Z Flag) is not equal to 0, meaning x was just set to zero by the dex call above))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Fill ROM size to exactly 4KB
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    org $FFFC       ; Need to complete ROM cartridge to FFFF, atari requires us to end with 4 bytes, these 4 bytes tell the atari where to go when the system is reset
    .word Start     ; reset vector - Start here being the Start Label. Whenever the system is reset it will find the Start label and go there
    .word Start     ; interrupt vector - unused by atari - .word lets us write 2 bytes, Start is a label (which is a memory position), labels are 16 bits (or 2 bytes)


