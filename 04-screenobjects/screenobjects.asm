
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Play field
;; 
;;   PF0     PF1     PF2      
;;   4567 76543210 01234567
;;  |====|========|========|========|=========|====|
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |====|========|========|========|=========|====|
;;
;;
;;  |==============================================|
;;  |                        
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    processor 6502

    include "VCS.H"
    include "macro.h"


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Define variables for use
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    seg.u Variables
    org $80
P0Height ds 1       ; define 1 byte for Player 0 height
P1Height ds 1       ; define 1 byte for Player 1 height

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Start the ROM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    seg code
    org $F000

Start:
    CLEAN_START

    ;; Set the background to blue
    ldx #$80
    stx COLUBK

    ;; Set the play field to white
    lda #%1111
    sta COLUPF

    ;; Set the heights of the players
    lda #10
    sta P0Height
    sta P1Height

NextFrame:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; VSYNC (3 lines) + VSYNC (37 lines)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    lda #02
    sta VBLANK
    sta VSYNC

    REPEAT 3
        sta WSYNC
    REPEND

    ldx #0
    stx VSYNC

    REPEAT 37
        sta WSYNC
    REPEND

    lda #0
    sta VBLANK


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Set playfield flags
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    lda #%00000000 ; Playfield mirroring
    sta CTRLPF

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Set player colours
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    lda #$48        ; light red
    sta COLUP0

    lda #$C6        ; light green
    sta COLUP1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Draw visible play area
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ;; Skip 10 scanlines
    REPEAT 10
        sta WSYNC
    REPEND

    ;; Draw the scoreboard

    ;;;; Set playfield to score rendering mode
    lda #%00000010
    sta CTRLPF

    ldy #0
ScoreboardLoop:
    lda Number2Bitmap,Y
    sta PF1
    sta WSYNC
    iny
    cpy #10
    bne ScoreboardLoop

    lda #0
    sta PF1

    ;;;; Disable score rendering
    lda #0
    sta CTRLPF

    ;; skip 50 lines
    REPEAT 50
        sta WSYNC
    REPEND

    ;; Draw player 0
    ldy #0
Player0Loop:
    lda PlayerBitmap,Y
    sta GRP0
    sta WSYNC
    iny
    cpy P0Height
    bne Player0Loop

    lda #0
    sta GRP0

    ;; Draw Player 1
    ldy #0
Player1Loop:
    lda PlayerBitmap,Y
    sta GRP1
    sta WSYNC
    iny
    cpy P1Height
    bne Player1Loop

    lda #0
    sta GRP1

    REPEAT 102
        sta WSYNC
    REPEND

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Overscan 30 lines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ldx #2
    stx VBLANK

    REPEAT 30
        sta WSYNC
    REPEND

    jmp NextFrame


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BITMAPS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Player Sprite
    org $FFE8
PlayerBitmap:
    .byte #%01111110    ;  ######
    .byte #%11111111    ; ########
    .byte #%10011001    ; #  ##  #  
    .byte #%11111111    ; ########
    .byte #%11111111    ; ########
    .byte #%11111111    ; ########
    .byte #%10111101    ; # #### #
    .byte #%11000011    ; ##    ##
    .byte #%11111111    ; ########
    .byte #%01111110    ;  ######

;; Number 2
    org $FFF2
Number2Bitmap:
    .byte #%00001110    ; ######
    .byte #%00001110    ; ######
    .byte #%00000010    ;     ##
    .byte #%00000010    ;     ##
    .byte #%00001110    ; ######
    .byte #%00001110    ; ######
    .byte #%00001000    ; ##
    .byte #%00001000    ; ##
    .byte #%00001110    ; ######
    .byte #%00001110    ; ######

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fill ROM space and set reset and interrupt vectors to the Start Label
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    org $FFFC
    .word Start
    .word Start
