    processor 6502

    include "VCS.H"
    include "macro.h"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Define variables for use
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    seg.u Variables
    org $80

PlayerHeight byte           ; Allocate 1 byte to store player height
PlayerYPos byte             ; Allocate 1 byte to store player Y position

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Start the ROM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    seg code
    org $F000

Start:
    CLEAN_START

    lda #9
    sta PlayerHeight

    lda #180
    sta PlayerYPos

NextFrame:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; VSYNC (3 lines) + VSYNC (37 lines)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    lda #02
    sta VBLANK
    sta VSYNC

    REPEAT 3
        sta WSYNC
    REPEND

    ldx #0
    stx VSYNC

    REPEAT 37
        sta WSYNC
    REPEND

    lda #0
    sta VBLANK


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Draw visible play area
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ldx #192

Scanline:
    txa                     ; Transfer X (scanline number) into A
    sec                     ; Set the carry flag since we are about to do subtraction
    sbc PlayerYPos          ; Subtract player Y position from the value in the accumulator (current scanline number)
    cmp PlayerHeight        ; Scanline number - Y position <= Player height (e.g. 
                            ; Scanline = 100, PlayerYPos = 95 GIVES 100 - 95 = 5 
                            ; which is less than the height of the player, so we know we are inside it
    bcc LoadBitmap          ; If we ARE inside the sprite then jump to the player loop rendering
    lda #0                  ; otherwise set the row of the bitmap to render to 0 (which is a blank row)

LoadBitmap:
    tay                     ; transfer the accumulator to y (the accumulator is now the result of scanline - player Y
                            ;  pos giving us the row of the bitmap to render OR it is 0 if we didn't branch here

    lda PlayerBitmap,Y      ; load the row of the bitmap to render into register a
    sta GRP0

    lda PlayerColour,Y      ; Load the colour of the current row into the accumulator
    sta COLUP0

    sta WSYNC

    dex
    bne Scanline


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Overscan 30 lines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ldx #2
    stx VBLANK

    REPEAT 30
        sta WSYNC
    REPEND


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Change the player Y position
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    lda PlayerYPos          ; Put current player position into the accumulator
    tax                     ; Transfer accumulator into X (puts current player position in x)
    dex                     ; Move the player down 1 pixel
    
    ;; If current position + player height is above 0 (i.e. sprite still on screen)
    adc PlayerHeight        ; Add player height to the current value in the accumulator
    sbc 1                   ; decrement the accumulator
    bne ChangeYPos          ; if is above 0 skip to changing the y position
    ldx #192                ; Otherwise set the player to be at the top of the screen again

ChangeYPos:
    stx PlayerYPos

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Next frame
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    jmp NextFrame

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Lookup table for the player bitmap and colour
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
PlayerBitmap:
    byte #%00000000    ;
    byte #%00101000    ;   # #
    byte #%01110100    ;  ### #
    byte #%11111010    ; ##### #
    byte #%11111010    ; ##### #
    byte #%11111010    ; ##### #
    byte #%11111110    ; #######
    byte #%01101100    ;  ## ##
    byte #%00110000    ;   ##

PlayerColour:
    byte #$00
    byte #$40
    byte #$40
    byte #$40
    byte #$40
    byte #$42
    byte #$42
    byte #$44
    byte #$D2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fill ROM space and set reset and interrupt vectors to the Start Label
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    org $FFFC
    .word Start
    .word Start
