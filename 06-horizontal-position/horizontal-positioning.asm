;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Horizontal positioning
;;
;;      68 colour clocks           160 colour clocks
;;  |====================|====================================================|
;;  |                    |                                                    | 
;;
;;  3 colour clocks PER cpu cycle
;;
;;  To horizontally position we have to:
;;   1. WSYNC to be at the start of the next scanline
;;   2. Wait until the electron beam reaches the point in the screen we want
;;      want the player to be positioned
;;   3. Strobe the RESP0 register
;;
;; To calucalte where the electron beam is....we need to use cpu cycles up
;; until it gets to the right place. The MINIMAL thing we can do is a loop
;; where we decrement a number, as follows:
;;
;;    ldx #5 ; To be in x position 5
;; Loop:
;;    dex       ; Uses 2 cpu cycles
;;    bne Loop  ; Uses 3 cpu cycles
;;
;; This means we can only position in groups of 5 cpu cycles....HOWEVER if we
;; remember that a cpu cycle is 3 colour clocks!!! This means the minimum
;; position we can set is 3 x 5 = 15 pixels.
;;
;; If we want an x position of 22, we have to remember to add on the 68 colour
;; clocks of the horizontal blank, which gives us 90, then we know we have to 
;; run 6 loop cycles before setting the position, since 90/15 = 6. So in the 
;; loop above we would set the x register to 6.
;;
;; This only gets us 15 pixel increments, which is oviously very poor jerky
;; movement....
;;
;; There is a special register we can set an adjustment to from -8 to +7 which
;; tells the TIA to draw the player at that offset from the set position of
;; RESP0
;;
;; So if we do the x position calculation (x + 68)/15 we can save the division
;; remainder, this remainder allows us to find the "fine offset".
;; 
;; Now our process becomes 
;;  0. strobe HMCLR (which clears the old horizontal position)
;;  1. WSYNC to start the next scanline
;;  2. Wait (x + 68)/15 cycles and save the remainder
;;  3. Use the remainder to find the fine offset
;;  4. Strobe RESP0 to set the low-res position
;;  5. Write the fine offset to HMP0 to set fine adjustment from -8 to 7
;;  6. Wait for the next scanline (sta WSYNC)
;;  7. Strobe HMOVE
;;
;; NOTES ON DIVISION:
;;
;; We cannot actually do division directly, instead in a loop we can subtract
;; the number from itself (we could also count loop iterations if we needed,
;; but we don't actually care about the number of iterations, only the
;; remainder)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    processor 6502

    include "VCS.H"
    include "macro.h"

    seg.u Variables
    org $80

PlayerHeight byte
PlayerYPos byte
PlayerXPos byte

    seg code
    org $F000

Start:
    CLEAN_START

    lda #10
    sta PlayerHeight

    lda #90
    sta PlayerYPos

    lda #0
    sta PlayerXPos

    lda #5
    sta COLUP0

NextFrame:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; VSYNC + BLANK
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    lda #2
    sta VBLANK
    sta VSYNC

    ;; VSYNC
    REPEAT 3
        sta WSYNC
    REPEND

    lda #0
    sta VSYNC

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Set player horizontal position
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Now our process becomes 
;; -1: Load the x pixel position we WANT to be in into the accumulator
;;  0. strobe HMCLR (which clears the old horizontal position)
;;  1. WSYNC to start the next scanline
;;  2. Wait (x + 68)/15 cycles and save the remainder
;;  3. Use the remainder to find the fine offset
;;  4. Strobe RESP0 to set the low-res position
;;  5. Write the fine offset to HMP0 to set fine adjustment from -8 to 7
;;  6. Wait for the next scanline (sta WSYNC)
;;  7. Strobe HMOVE
    lda PlayerXPos

    sta HMCLR
    sta WSYNC

    sec              ; Set the carry flag before subtraction                                    ; 2 cycles
DivideLoop:
    sbc #15         ; Subtract 15 from the accumulator                                          ; 2 cycles 
                    ; (remember 3 colour clocks per cpu cycle * 5 cycles per loop)
    bcs DivideLoop  ; Loop while the carry flag is still set                                    ; 3 cycles when branch
                                                                                                ; 2 when not

    ;; In the 4 final bits of the number we end up with a number between
    ;; -8 and 7 (remembering the number is stored as twos complement)
    eor #7                                                                                      ; 2 cycles
    asl                                                                                         ; 2 cycles
    asl                                                                                         ; 2 cycles
    asl                                                                                         ; 2 cycles
    asl                                                                                         ; 2 cycles

    sta HMP0        ; Set fine position                                                         ; 3 cycles
    sta RESP0       ; Set low-res position                                                      ; 3 cycles
    sta WSYNC                                                                                   ; 3 cycles
    sta HMOVE

    ;; 78 cpu cycles per scanline
    ;; without the loop above we have 21 cycles
    ;; each iteration round the loop is 5 cycles. 
    ;; x position is from 0 to 160 giving between 1 and 11 loop iterations
    ;; in turn giving between 4 and 54 cpu cycles (the final loop iteration has 1 fewer cpu cycles because of bcs)
    ;; this is a total of between 28 and 75 cycles
    ;; This means we always use 3 full scanlines to perform the horizontal sync

    REPEAT 31
        sta WSYNC
    REPEND

    ;; Disable VBLANK
    lda #0
    sta VBLANK

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Visible scanlines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    REPEAT 90
        sta WSYNC
    REPEND

    ldy #9
DrawPlayerLoop:
    lda PlayerBitmap,Y
    sta GRP0
    sta WSYNC
    dey
    bne DrawPlayerLoop

    lda #0
    sta GRP0

    REPEAT 92
        sta WSYNC
    REPEND

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Overscan
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    REPEAT 30
        sta WSYNC
    REPEND

    ldx PlayerXPos
    inx
    cpx #160
    bcc StoreXPosition
    ldx #0

StoreXPosition:
    stx PlayerXPos


    jmp NextFrame

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bitmaps
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PlayerBitmap:
    byte #%00000000     ;
    byte #%01000100     ;  #   # 
    byte #%01000100     ;  #   # 
    byte #%01010100     ;  # # # 
    byte #%00111000     ;   ###  
    byte #%01101100     ;  ## ## 
    byte #%11000110     ; ##   ## 
    byte #%01101100     ;  ## ## 
    byte #%00111000     ;   ###  
    byte #%00010000     ;    # 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Pad cart to 4k and set reset and interrupt vectors
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    org $FFFC
    .word Start
    .word Start
