	processor 6502
	include "VCS.h"
	include "macro.h"

	seg.u variables
	org $80

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Gamestate bitmask
;; bit 1 - gameover (rightmost bit)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
GameState byte
ExplosionP0 byte
ExplosionP1 byte
PewPewP0 byte
PewPewP1 byte

	seg code
	org $F000

Start:
	CLEAN_START

	ldy #0
	sty ExplosionP0
	sty ExplosionP1
	sty PewPewP0
	sty PewPewP1
	sty GameState

NextFrame:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; VSYNC + VBLANK
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	lda #2
	sta VBLANK
	sta VSYNC

	REPEAT 3
		sta WSYNC
	REPEND

	lda #0
	sta VSYNC

	REPEAT 37
		sta WSYNC
	REPEND

	lda #0
	sta VBLANK

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; VISIBLE SCANLINES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	; ldx #3
	; stx AUDV0

	REPEAT 48
		sta WSYNC
		sta WSYNC
		sta WSYNC
		sta WSYNC
	REPEND

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Overscan
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	lda #2
	sta VBLANK

CheckP0Up:
	lda #%00010000
	bit SWCHA
	bne CheckP0Down

	;; If we are in gameover state ignore this
	lda #%00000001
	bit GameState
	bne CheckP0Down

	;; Load the current frequency of the explosion into a
	lda ExplosionP0

	;; If it's NOT 0 we are already playing an explosion so just skip
	cmp #0
	bne CheckP0Down

	;; Otherwise start an explosion sound by setting the freq to 1, the
	;; volume to 3, and the sound to 8 and set the gameover gamestate
	jsr StopAllSounds
	lda #1
	sta ExplosionP0
	lda #3
	sta AUDV0
	lda #8
	sta AUDC0

	;; Set Gameover state (bit 1 in GameState)
	lda GameState
	ora #%00000001
	sta GameState

CheckP0Down:
	lda #%00100000
	bit SWCHA
	bne CheckP0Left
	
	;; If we are in gameover state ignore this
	lda #%00000001
	bit GameState
	bne CheckP0Left

	;; If we are already making a pew pew sound don't start another
	lda PewPewP0
	cmp #0
	bne CheckP0Left

	;; Otherwise start a pew pew sound by setting the pew pew counter, and sound
	;; to 1 and the volume to 3, and the sound to 1
	lda #1
	sta PewPewP0
	sta AUDC0
	lda #3
	sta AUDV0

CheckP0Left:
	lda #%01000000
	bit SWCHA
	bne CheckP0Right

	;; If we are in gameover state ignore this
	lda #%00000001
	bit GameState
	bne CheckP0Right

	;; Load the current frequency of the explosion into a
	lda ExplosionP1

	;; If it's NOT 0 we are already playing an explosion so just skip
	cmp #0
	bne CheckP0Right

	;; Otherwise start an explosion sound by setting the freq to 1, the
	;; volume to 3, and the sound to 8 and set the gameover gamestate
	jsr StopAllSounds
	lda #1
	sta ExplosionP1
	lda #3
	sta AUDV1
	lda #8
	sta AUDC1

	;; Set Gameover state (bit 1 in GameState)
	lda GameState
	ora #%00000001
	sta GameState

CheckP0Right:
	lda #%10000000
	bit SWCHA
	bne NoInput

	;; If we are in gameover state ignore this
	lda #%00000001
	bit GameState
	bne NoInput

	;; If we are already making a pew pew sound don't start another
	lda PewPewP1
	cmp #0
	bne NoInput

	;; Otherwise start a pew pew sound by setting the pew pew counter, and sound
	;; to 1 and the volume to 3, and the sound to 1
	lda #1
	sta PewPewP1
	sta AUDC1
	lda #3
	sta AUDV1

NoInput:

	sta WSYNC

	jsr PlayExplosionP0
	
	sta WSYNC

	jsr PlayExplosionP1

	sta WSYNC

	jsr PlayPewPewP0

	sta WSYNC

	jsr PlayPewPewP1

	sta WSYNC

	REPEAT 25
		sta WSYNC
	REPEND

	jmp NextFrame

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to play  Explosion Sound for P0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
PlayExplosionP0 subroutine
	;; Check for explosion
	;; Load the current value of the explosion frequency into the accumulator
	lda ExplosionP0
	;; If it's 0 we don't want to play the sound so skip to no explosion
	;; ELSE If it's not 0 set the frequency to the current value before incrementing
	cmp	#0
	beq .NoExplosionP0

	;; If the value is > 31 we want to play the lingering explosion sound
	tax
	cpx #31
	bmi .SetFrequencyP0
	ldx #31

.SetFrequencyP0
	stx AUDF0

	;; Add 1 to the frequency and then EOR it with 32 (so once we reach 32 we
	;; set back to 0 and turn off the sound)
	clc
	adc #1
	and #63

	;; If we DID just set back to 0 turn off the volume to the voice
	bne .ContinuePlayingExplosionP0
	sta AUDV0
	sta AUDF0

.ContinuePlayingExplosionP0

	sta ExplosionP0

.NoExplosionP0:

	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to play  Explosion Sound for P1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
PlayExplosionP1 subroutine
	;; Check for explosion
	;; Load the current value of the explosion frequency into the accumulator
	lda ExplosionP1
	;; If it's 0 we don't want to play the sound so skip to no explosion
	;; ELSE If it's not 0 set the frequency to the current value before incrementing
	cmp	#0
	beq .NoExplosionP1

	;; If the value is > 31 we want to play the lingering explosion sound
	tax
	cpx #31
	bmi .SetFrequencyP1
	ldx #31

.SetFrequencyP1
	stx AUDF1

	;; Add 1 to the frequency and then EOR it with 32 (so once we reach 32 we
	;; set back to 0 and turn off the sound)
	clc
	adc #1
	and #63

	;; If we DID just set back to 0 turn off the volume to the voice
	bne .ContinuePlayingExplosionP1
	sta AUDV1
	sta AUDF1

.ContinuePlayingExplosionP1

	sta ExplosionP1

.NoExplosionP1:

	rts


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to play PewPew Sound for P0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
PlayPewPewP0 subroutine
	;; If we are in gameover state just continue
	lda #%00000001
	bit GameState
	bne .NoPewPewP0

	;; Load the current pew pew state
	;; If it's 0 we aren't making a pew pew so just skip
	lda PewPewP0
	cmp #0
	beq .NoPewPewP0

	;; Otherwise write the current state to the audio frequency
	sta AUDF0

	;; Now add 1 to it, if we get above 31 then the sound is over
	clc
	adc #1
	and #31

	bne .ContinuePewPewP0
	lda #0
	sta AUDF0
	sta AUDV0

.ContinuePewPewP0:
	sta PewPewP0

.NoPewPewP0:
	rts


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to play PewPew Sound for P1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
PlayPewPewP1 subroutine
	;; If we are in gameover state just continue
	lda #%00000001
	bit GameState
	bne .NoPewPewP1

	;; Load the current pew pew state
	;; If it's 0 we aren't making a pew pew so just skip
	lda PewPewP1
	cmp #0
	beq .NoPewPewP1

	;; Otherwise write the current state to the audio frequency
	sta AUDF1

	;; Now add 1 to it, if we get above 31 then the sound is over
	clc
	adc #1
	and #31

	bne .ContinuePewPewP1
	lda #0
	sta AUDF1
	sta AUDV1

.ContinuePewPewP1:
	sta PewPewP1

.NoPewPewP1:
	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Stop all sounds
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

StopAllSounds subroutine
	lda #0
	sta AUDC0
	sta AUDC1
	sta AUDF0
	sta AUDF1
	sta AUDV0
	sta AUDV1
	sta PewPewP0
	sta PewPewP1
	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fill ROM and set reset and interrupt vectors
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	org $FFFC
	.word Start
	.word Start
