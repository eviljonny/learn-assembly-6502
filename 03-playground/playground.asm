
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Play field
;; 
;;   PF0     PF1     PF2      
;;   4567 76543210 01234567
;;  |====|========|========|========|=========|====|
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |    |        |        |        |         |    |
;;  |====|========|========|========|=========|====|
;;
;;
;;  |==============================================|
;;  |                        
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    processor 6502

    include "VCS.H"
    include "macro.h"

    seg code
    org $F000

Start:
    CLEAN_START

    ;; Set the background to blue
    ldx #$80
    stx COLUBK

    ;; Set the play field to yellow
    lda #$1C
    sta COLUPF

NextFrame:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; VSYNC (3 lines) + VSYNC (37 lines)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    lda #02
    sta VBLANK
    sta VSYNC

    REPEAT 3
        sta WSYNC
    REPEND

    ldx #0
    stx VSYNC

    REPEAT 37
        sta WSYNC
    REPEND

    lda #0
    sta VBLANK


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Set playfield flags
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    lda #%00000001
    sta CTRLPF



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Draw visible play area
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ;; Skip 7 scanlines with PF0, PF1, PF2 set to 00000000
    lda #0
    sta PF0
    sta PF1
    sta PF2
    REPEAT 7
        sta WSYNC
    REPEND

    ;; Set PF0 to 1110 and PF1, PF2 to 11111111
    lda #%11100000
    sta PF0
    lda #%11111111
    sta PF1
    sta PF2
    ;; 7 scanlines
    REPEAT 7
        sta WSYNC
    REPEND

    ;; Set PF0 to 0010 and PF1, PF2 to 00000000
    lda #%01100000
    sta PF0
    lda #0
    sta PF1
    lda #%10000000
    sta PF2
    ;; 164 scanlines
    REPEAT 164
        sta WSYNC
    REPEND

    ;; Set PF0 to 1110 and PF1, PF2 to 11111111
    lda #%11100000
    sta PF0
    lda #%11111111
    sta PF1
    sta PF2
    ;; 7 scanlines
    REPEAT 7
        sta WSYNC
    REPEND

    ;; Set PF0, PF1, PF2 to 00000000
    ;; 7 scanlines
    lda #0
    sta PF0
    sta PF1
    sta PF2

    ;; 7 scanlines
    REPEAT 7
        sta WSYNC
    REPEND

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Overscan 30 lines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ldx #2
    stx VBLANK

    REPEAT 30
        sta WSYNC
    REPEND

    jmp NextFrame

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fill ROM space and set reset and interrupt vectors to the Start Label
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    org $FFFC
    .word Start
    .word Start
