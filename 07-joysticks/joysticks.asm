;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Register SWCHA - $280 (in PIA) - Joysticks
;; Register SWCHB - $282 (in PIA) - Console front panel switches
;;
;; Register SWCHA (0 means ACTIVE (pressed)):
;;
;;     P0      P1
;;   |-----| |-----|
;;   
;;   1 0 1 1 1 1 1 1 
;;   | | | | | | | |- P1 Up
;;   | | | | | | |--- P1 Down
;;   | | | | | |----- P1 Left
;;   | | | | |------- P1 Right
;;   | | | |
;;   | | | |-- P0 Up
;;   | | |---- P0 Down
;;   | |------ P0 Left    - PRESSED
;;   |-------- P0 Right

    processor 6502

    include "VCS.H"
    include "macro.h"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine list
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CheckP0Joystick subroutine
;; CheckP1Joystick subroutine
;; SetPlayer0XPos subroutine
;; SetPlayer1XPos subroutine
;; InitiateExplosion subroutine
;; PlayExplosionSound subroutine
;; PlayPewPewP0 subroutine
;; PlayPewPewP1 subroutine
;; WinP0 subroutine
;; WinP1 subroutine
;; MoveMissle subroutine
;; CheckFireButtons subroutine
;; CheckForHits subroutine
;; AdvanceExplosionFrame subroutine
;; SetScanlineOffsets subroutine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MACRO list
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  MAC DRAW_MISSLES
;;  MAC SET_MISSLE_STATE
;;  MAC SET_MISSLE_COLOURS
;;  MAC DRAW_PLAYER0_SPRITE
;;  MAC DRAW_PLAYER1_SPRITE
;;  MAC LOAD_PLAYER0_SPRITE sprite_index
;;  MAC LOAD_PLAYER1_SPRITE sprite_index
;;  MAC PLAYER0_SCANLINE sprite_index_next_line
;;  MAC PLAYER1_SCANLINE sprite_index_next_line
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Variables
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    seg.u variables
    org $80

AUDIO_VOLUME = 12
X_MIN = 0
X_MAX = 153
DISTANCE_BETWEEN_PLAYERS = 114
EXPLOSION_LENGTH = 7
PLAYER_HEIGHT = 9

Y_MIN_P0 = 4
Y_MAX_P0 = 84

Y_MIN_P1 = 99
Y_MAX_P1 = 179


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DRAW_MISSLES
;;
;; cycles: 12
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC DRAW_MISSLES
        lda DrawMissle0 ; 3
        sta ENAM0       ; 3
        lda DrawMissle1 ; 3
        sta ENAM1       ; 3
    ENDM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END DRAW_MISSLES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SET_MISSLE_STATE
;;
;; cycles:
;;    25 if no missles displayed
;;    27 if 1 missle displayed
;;    29 if 2 missles displayed
;;
;; Sets the variables DrawMissle0 and DrawMissle1 to the appropriate value
;; to be used on the next scanline based on the var CurrentScanline
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC SET_MISSLE_STATE
        ;; Turn both missles on
        lda #2                          ; 2
        sta DrawMissle0                 ; 3
        sta DrawMissle1                 ; 3

        lda #0                          ; 2
        ldx CurrentScanline             ; 3

        cpx Missle0YPos                 ; 3
        beq .CheckMissle1               ; 2/3
        sta DrawMissle0                 ; 3

.CheckMissle1
        cpx Missle1YPos                 ; 3
        beq .FinishedCheckingMissles    ; 2/3
        sta DrawMissle1                 ; 3

.FinishedCheckingMissles
    ENDM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END SET_MISSLE_STATE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SET_MISSLE_COLOURS
;;
;; cycles: 10
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC SET_MISSLE_COLOURS
        lda #$1E      ; Yellow
        sta COLUP0
        lda #$DC
        sta COLUP1    ; Green
    ENDM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END SET_MISSLE_COLOURS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DRAW_PLAYER0_SPRITE
;;
;; cycles: 6
;;
;; Requires a to be preloaded with the sprite byte and x with the colour
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC DRAW_PLAYER0_SPRITE
        sta GRP0         ; 3
        stx COLUP0       ; 3
    ENDM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END DRAW_PLAYER0_SPRITE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DRAW_PLAYER1_SPRITE
;;
;; cycles: 6
;;
;; Requires a to be preloaded with the sprite byte and x with the colour
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC DRAW_PLAYER1_SPRITE
        sta GRP1         ; 3
        stx COLUP1       ; 3
    ENDM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END DRAW_PLAYER1_SPRITE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LOAD_PLAYER0_SPRITE
;;
;; cycles: 14/16 (depends on crossing page boundaries)
;;
;; Load player 0 sprite byte {0} into the accumulator and player 0
;; colour byte {1} into the x register
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC LOAD_PLAYER0_SPRITE ; usage LOAD_PLAYER1_SPRITE 3 p1 sprite line 3)
        ldy #{1}                    ; 2
        lda (Player0ColourPtr),Y    ; 5 (+1 on cross page boundary)
        tax                         ; 2
        lda (Player0SpritePtr),Y    ; 5 (+1 on cross page boundary)
    ENDM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END LOAD_PLAYER0_SPRITE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LOAD_PLAYER1_SPRITE
;;
;; cycles: 14/16 (depends on crossing page boundaries)
;;
;; Load player 1 sprite byte {1} into the accumulator and player 1
;; colour byte {1} into the x register
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC LOAD_PLAYER1_SPRITE ; usage LOAD_PLAYER1_SPRITE 3 p1 sprite line 3)
        ldy #{1}                    ; 2
        lda (Player1ColourPtr),Y    ; 5 (+1 on cross page boundary)
        tax                         ; 2
        lda (Player1SpritePtr),Y    ; 5 (+1 on cross page boundary)
    ENDM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END LOAD_PLAYER1_SPRITE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PLAYER0_SCANLINE
;;
;; cycles: 65-71 (18 of which MUST be completed in HBLANK)
;;
;; Draw a scanline including both the missles and the already loaded byte of
;; the player 0 sprite.
;; Also calculate the missle state and load the player 0 sprite byte {1} for the
;; following scanline
;; Finally complete the current scanline
;;
;; Params:
;;   {1} - The sprite byte index to load for the following scanline
;;    a  - The sprite byte to display on the current scanline
;;    x  - The sprite colour to display on the current scanline
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC PLAYER0_SCANLINE ; usage PLAYER1_SCANLINE x (where x will be the sprite position offset)
        DRAW_PLAYER0_SPRITE             ; 6
        DRAW_MISSLES                    ; 12
        dec CurrentScanline             ; 5
        SET_MISSLE_STATE                ; 25-29
        LOAD_PLAYER0_SPRITE {1}         ; 14-16
        sta WSYNC
    ENDM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END PLAYER0_SCANLINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PLAYER1_SCANLINE
;;
;; cycles: 65-71 (18 of which MUST be completed in HBLANK)
;;
;; Draw a scanline including both the missles and the already loaded byte of
;; the player 0 sprite.
;; Also calculate the missle state and load the player 0 sprite byte {1} for the
;; following scanline.
;; Finally complete the current scanline
;;
;; Params:
;;   {1} - The sprite byte index to load for the following scanline
;;    a  - The sprite byte to display on the current scanline
;;    x  - The sprite colour to display on the current scanline
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MAC PLAYER1_SCANLINE ; usage PLAYER1_SCANLINE x (where x will be the sprite position offset)
        DRAW_PLAYER1_SPRITE
        DRAW_MISSLES
        dec CurrentScanline
        SET_MISSLE_STATE
        LOAD_PLAYER1_SPRITE {1}
        sta WSYNC
    ENDM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END PLAYER1_SCANLINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; GameState is a bitmask
;; bit 7 - 
;; bit 6 - 
;; bit 5 - 
;; bit 4 - 
;; bit 3 - 
;; bit 2 - P1 won
;; bit 1 - P0 won
;; bit 0 - GameOver
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
GameState byte

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;                     -----------------------------------------
;;                  |  |                                       |
;;   ScanlinesToP1  |  |                                       |
;;                  |  |                                       |
;;                     |                 xxx                   |
;;         9           |                 xxx                   |
;;                     |                 xxx                   |
;;                  |  |                                       |
;;   ScanlinesToP0  |  |                                       |
;;                  |  |                                       |
;;                     |             xxx                       | 
;;         9           |             xxx                       | 
;;                     |             xxx                       | 
;;                  |  |                                       |
;;     Player0YPos  |  |                                       |
;;                  |  |                                       |
;;                     -----------------------------------------
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ScanlinesToP1 byte
ScanlinesToP0 byte

Player0XPos byte
Player0YPos byte

Player1XPos byte
Player1YPos byte

Missle0XPos byte
Missle0YPos byte

Missle1XPos byte
Missle1YPos byte

Player0SpritePtr word     ; Pointer to the current sprite0 lookup table
Player1SpritePtr word     ; Pointer to the current sprite0 lookup table

Player0ColourPtr word     ; Pointer to the colour lookup table
Player1ColourPtr word     ; Pointer to the colour lookup table

DrawMissle0 byte
DrawMissle1 byte
CurrentScanline byte

;; NEED TO ACCOUNT FOR THE FINE OFFSET WHEN CALCULATING HITS....OR FIX TO USE
;; BUILT IN COLLISION DETECTION

ExplosionFrame byte

TimeWaster byte
ExplosionDelay byte

ExplosionSound byte

PewPewP0 byte
PewPewP1 byte

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ROM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    seg code
    org $F000

Start:
    CLEAN_START

    lda #$00        ; Black
    sta COLUBK

    lda #$0F        ; White
    sta COLUPF

    lda #74
    sta Player0XPos
    sta Player1XPos

    lda #153
    sta Player1YPos
    lda #30
    sta Player0YPos

    jsr SetScanlineOffsets

    lda #-1
    sta Missle0YPos
    sta Missle1YPos

    lda #0
    sta ExplosionFrame

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Init the pointers to the correct lookup tables
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ;; Remember that memroy addresses are 2 bytes long, so we need to get the
    ;; Low byte of the memory address for the centre sprite and store it in 
    ;; the first byte of the Sprite Pointer word.
    ;; Then we need to get the high byte of the memory address for the centre
    ;; sprite and store it in the second byte of the sprite pointer word
    lda #<P0Sprite               ; Get the low byte of the memory address
    sta Player0SpritePtr         ; Store it in the low byte of the pointer
    lda #>P0Sprite               ; Get the high byte of the memory address
    sta Player0SpritePtr+1       ; Store it in the high byte of the pointer

    lda #<P0SpriteColour
    sta Player0ColourPtr
    lda #>P0SpriteColour
    sta Player0ColourPtr+1

    lda #<P1Sprite
    sta Player1SpritePtr
    lda #>P1Sprite
    sta Player1SpritePtr+1

    lda #<P1SpriteColour
    sta Player1ColourPtr
    lda #>P1SpriteColour
    sta Player1ColourPtr+1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; VSYNC + VBLANK
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
NextFrame:
    lda #2
    sta VBLANK
    sta VSYNC

    ;; VSYNC
    REPEAT 3
        sta WSYNC
    REPEND

    lda #0
    sta VSYNC


    sta HMCLR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Set Player0XPosition
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    jsr SetPlayer0XPos

    sta WSYNC

    ;; If x was > 135 we need to wait for another scanline to finish since we used an extra one in the div15 loops
    clc
    lda Player0XPos
    cmp #135
    bcs HPosP0NoSkipLine
    sta WSYNC

HPosP0NoSkipLine:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Set Player1XPosition
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    jsr SetPlayer1XPos

    sta WSYNC

    ;; If x was > 135 we need to wait for another scanline to finish since we used an extra one in the div15 loops
    clc
    lda Player1XPos
    cmp #135
    bcs HPosP1NoSkipLine
    sta WSYNC

HPosP1NoSkipLine:

    ;; VBLANK
    REPEAT 30
        sta WSYNC
    REPEND

    sta WSYNC
    sta HMOVE

    lda #0
    sta VBLANK

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Visible scanlines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Scanlines before Player 1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    lda #192
    sta CurrentScanline
    SET_MISSLE_COLOURS

;; Count down player 0 offset doing only missle loading
    ldy ScanlinesToP1
ScanlinesBeforeP1:
    DRAW_MISSLES
    dec CurrentScanline
    SET_MISSLE_STATE
    dey
    sta WSYNC
    bne ScanlinesBeforeP1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Player1 scanlines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    LOAD_PLAYER1_SPRITE 8

P1Scanlines:
    ; Scanline1
    PLAYER1_SCANLINE 7
    PLAYER1_SCANLINE 6
    PLAYER1_SCANLINE 5
    PLAYER1_SCANLINE 4
    PLAYER1_SCANLINE 3
    PLAYER1_SCANLINE 2
    PLAYER1_SCANLINE 1
    PLAYER1_SCANLINE 0
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Scanlines between the players
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    DRAW_PLAYER1_SPRITE ; clear the player1 graphics
    SET_MISSLE_COLOURS
    ldy ScanlinesToP0
    dey
ScanlinesBetweenPlayers:
    DRAW_MISSLES
    dec CurrentScanline
    SET_MISSLE_STATE
    dey
    sta WSYNC
    bne ScanlinesBetweenPlayers

    LOAD_PLAYER0_SPRITE 8

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Player 0 scanlines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
P0Scanlines:
    PLAYER0_SCANLINE 7
    PLAYER0_SCANLINE 6
    PLAYER0_SCANLINE 5
    PLAYER0_SCANLINE 4
    PLAYER0_SCANLINE 3
    PLAYER0_SCANLINE 2
    PLAYER0_SCANLINE 1
    PLAYER0_SCANLINE 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Remaining scanlines after player0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    DRAW_PLAYER0_SPRITE ; clear the player1 graphics
    SET_MISSLE_COLOURS
    ldy Player0YPos
    dey
ScanlinesAfterPlayers
    DRAW_MISSLES
    dec CurrentScanline
    SET_MISSLE_STATE
    dey
    sta WSYNC
    bne ScanlinesAfterPlayers

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Overscan - Section 1 all game, Section 2 game over only
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ;; If we are in gameover state ignore this
    lda #%00000001
    bit GameState
    bne GameOverState

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Joystick check - during Overscan
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; Start out by setting the sprites to be centered again. Then if no input
    ;; is pressed we will show the centered sprites
    lda #<P0Sprite
    sta Player0SpritePtr
    lda #>P0Sprite
    sta Player0SpritePtr+1
    lda #<P1Sprite
    sta Player1SpritePtr
    lda #>P1Sprite
    sta Player1SpritePtr+1

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; Move Missles
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    jsr MoveMissle
    sta WSYNC

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; Input checks
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    jsr CheckP0Joystick
    sta WSYNC
    jsr CheckP1Joystick
    sta WSYNC
    jsr CheckFireButtons
    sta WSYNC

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; In-game sounds
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    jsr PlayPewPewP0
    sta WSYNC
    jsr PlayPewPewP1
    sta WSYNC

    jsr CheckForHits       ; 18 when no hits
    jsr SetScanlineOffsets ; 32 (26 for sub + 6 for jsr)

    jmp NotGameOver

GameOverState:
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; In-game sounds - game over
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    jsr SetScanlineOffsets ; 32 (26 for sub + 6 for jsr)
    jsr PlayExplosionSound
    sta WSYNC
    jsr AdvanceExplosionFrame
    sta WSYNC

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Actual Overscan
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
NotGameOver:
    REPEAT 19
        sta WSYNC
    REPEND

    jmp NextFrame



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to Check P0 Joystick
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
CheckP0Joystick subroutine
.CheckP0Up:                                         ;; 7/8 (pressed/unpressed)
    lda #%00010000                      ;; 2
    bit SWCHA                           ;; 3
    bne .CheckP0Down                     ;; 2/3 (pressed/unpressed)

    lda Player0YPos
    cmp #Y_MAX_P0
    beq .CheckP0Down

    clc
    adc #2
    sta Player0YPos

.CheckP0Down:                                       ;; 7/8 (pressed/unpressed)
    lda #%00100000                      ;; 2
    bit SWCHA                           ;; 3
    bne .CheckP0Left                     ;; 2/3 (pressed/unpressed)

    lda Player0YPos
    cmp #Y_MIN_P0
    beq .CheckP0Left

    sec
    sbc #2
    sta Player0YPos

.CheckP0Left:                                       ;; 19/15/8 (pressed at min/pressed not min/unpressed)
    sta WSYNC

    lda #%01000000                      ;; 2
    bit SWCHA                           ;; 3
    bne .CheckP0Right                    ;; 2/3 (pressed/unpressed)

    lda #<P0SpriteLeft
    sta Player0SpritePtr
    lda #>P0SpriteLeft
    sta Player0SpritePtr+1

    ;; Reduce X Pos unless its = 10
    lda Player0XPos                     ;; 2
    cmp #X_MIN                            ;; 3
    beq .CheckP0Right                    ;; 2/3 (not at min/at min)

    sbc #1                              ;; 2
    sta Player0XPos                     ;; 3

.CheckP0Right:                                      ;; 19/15/8 (pressed at min/pressed not min/unpressed)
    lda #%10000000                      ;; 2
    bit SWCHA                           ;; 3
    bne .NoInputP0                       ;; 2/3 (pressed/unpressed)

    lda #<P0SpriteRight
    sta Player0SpritePtr
    lda #>P0SpriteRight
    sta Player0SpritePtr+1

    ;; Increase X Pos unless it's = 180
    lda Player0XPos                     ;; 2
    cmp #X_MAX                            ;; 3
    beq .NoInputP0                       ;; 2/3 (not at max/at max)

    adc #1                              ;; 2
    sta Player0XPos                     ;; 3

.NoInputP0:
    rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to Check P1 Joystick
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
CheckP1Joystick subroutine
.CheckP1Up:                                         ;; 7/8 (pressed/unpressed)
    lda #%00000001                      ;; 2
    bit SWCHA                           ;; 3
    bne .CheckP1Down                     ;; 2/3 (pressed/unpressed)

    lda Player1YPos
    cmp #Y_MAX_P1
    beq .CheckP1Down

    clc
    adc #2
    sta Player1YPos

.CheckP1Down:                                       ;; 7/8 (pressed/unpressed)
    lda #%00000010                      ;; 2
    bit SWCHA                           ;; 3
    bne .CheckP1Left                     ;; 2/3 (pressed/unpressed)

    lda Player1YPos
    cmp #Y_MIN_P1
    beq .CheckP1Left

    sec
    sbc #2
    sta Player1YPos

.CheckP1Left:                                       ;; 19/15/8 (pressed at min/pressed not min/unpressed)
    sta WSYNC

    lda #%00000100                      ;; 2
    bit SWCHA                           ;; 3
    bne .CheckP1Right                    ;; 2/3 (pressed/unpressed)

    lda #<P1SpriteLeft
    sta Player1SpritePtr
    lda #>P1SpriteLeft
    sta Player1SpritePtr+1

    ;; Reduce X Pos unless its = 10
    lda Player1XPos                     ;; 2
    cmp #X_MIN                            ;; 3
    beq .CheckP1Right                    ;; 2/3 (not at min/at min)

    sbc #1                              ;; 2
    sta Player1XPos                     ;; 3

.CheckP1Right:                                      ;; 19/15/8 (pressed at min/pressed not min/unpressed)
    lda #%00001000                      ;; 2
    bit SWCHA                           ;; 3
    bne .NoInputP1                       ;; 2/3 (pressed/unpressed)

    lda #<P1SpriteRight
    sta Player1SpritePtr
    lda #>P1SpriteRight
    sta Player1SpritePtr+1

    ;; Increase X Pos unless it's = 180
    lda Player1XPos                     ;; 2
    cmp #X_MAX                            ;; 3
    beq .NoInputP1                       ;; 2/3 (not at max/at max)

    adc #1                              ;; 2
    sta Player1XPos                     ;; 3

.NoInputP1:
    rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to set the X position of Player0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetPlayer0XPos subroutine
    lda Player0XPos
    sec
    sta WSYNC
    sta TimeWaster
.Div15Loop
    sbc #15
    bcs .Div15Loop
    eor #7
    asl
    asl
    asl
    asl
    sta HMP0
    sta RESP0

    rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END sub
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to set the X position of Player1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetPlayer1XPos subroutine
    lda Player1XPos
    sec
    sta WSYNC
    sta TimeWaster
.Div15Loop
    sbc #15
    bcs .Div15Loop
    eor #7
    asl
    asl
    asl
    asl
    sta HMP1
    sta RESP1

    rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END sub
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; InitiateExplosion
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
InitiateExplosion subroutine
    lda #0
    sta AUDC0
    sta AUDC1
    sta AUDF0
    sta AUDF1
    sta AUDV0
    sta AUDV1
    sta PewPewP0
    sta PewPewP1
    sta ExplosionDelay

    lda #1
    sta ExplosionSound
    lda #AUDIO_VOLUME
    sta AUDV0
    lda #8
    sta AUDC0

    lda #EXPLOSION_LENGTH
    sta ExplosionFrame

    lda #-1
    sta Missle0YPos
    sta Missle1YPos

    rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END InitiateExplosion
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PlayExplosionSound
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
PlayExplosionSound subroutine
    ;; Check for explosion
    ;; Load the current value of the explosion frequency into the accumulator
    lda ExplosionSound
    ;; If it's 0 we don't want to play the sound so skip to no explosion
    ;; ELSE If it's not 0 set the frequency to the current value before incrementing
    cmp  #0
    beq .NoExplosion

    ;; If the value is > 31 we want to play the lingering explosion sound
    tax
    cpx #31
    bmi .SetFrequency
    ldx #31

.SetFrequency
    stx AUDF0

    ;; Add 1 to the frequency and then EOR it with 32 (so once we reach 32 we
    ;; set back to 0 and turn off the sound)
    clc
    adc #1
    and #63

    ;; If we DID just set back to 0 turn off the volume to the voice
    bne .ContinuePlayingExplosion
    sta AUDV0
    sta AUDF0

.ContinuePlayingExplosion
    sta ExplosionSound

.NoExplosion:
    rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END PlayExplosionSound sub
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to play PewPew Sound for P0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
PlayPewPewP0 subroutine
    ;; If we are in gameover state just continue
    lda #%00000001
    bit GameState
    bne .NoPewPewP0

    ;; Load the current pew pew state
    ;; If it's 0 we aren't making a pew pew so just skip
    lda PewPewP0
    cmp #0
    beq .NoPewPewP0

    ;; Otherwise write the current state to the audio frequency
    sta AUDF0

    ;; Now add 1 to it, if we get above 31 then the sound is over
    clc
    adc #1
    and #31

    bne .ContinuePewPewP0
    lda #0
    sta AUDF0
    sta AUDV0

.ContinuePewPewP0:
    sta PewPewP0

.NoPewPewP0:
    rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to play PewPew Sound for P1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
PlayPewPewP1 subroutine
    ;; If we are in gameover state just continue
    lda #%00000001
    bit GameState
    bne .NoPewPewP1

    ;; Load the current pew pew state
    ;; If it's 0 we aren't making a pew pew so just skip
    lda PewPewP1
    cmp #0
    beq .NoPewPewP1

    ;; Otherwise write the current state to the audio frequency
    sta AUDF1

    ;; Now add 1 to it, if we get above 31 then the sound is over
    clc
    adc #1
    and #31

    bne .ContinuePewPewP1
    lda #0
    sta AUDF1
    sta AUDV1

.ContinuePewPewP1:
    sta PewPewP1

.NoPewPewP1:
    rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to initite P0 winning
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
WinP0 subroutine
    ;; Change all current sounds to explosion
    jsr InitiateExplosion

    ;; Change the colour pointer to point to the explosion colours
    lda #<ExplosionColour
    sta Player1ColourPtr
    lda #>ExplosionColour
    sta Player1ColourPtr+1

    ;; Set Gameover state (bit 1 in GameState, bit 2 for p0 win)
    lda GameState
    ora #%00000011
    sta GameState
 
    rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to initite P1 winning
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
WinP1 subroutine
    ;; Change all current sounds to explosion
    jsr InitiateExplosion

    ;; Change the colour pointer to point to the explosion colours
    lda #<ExplosionColour
    sta Player0ColourPtr
    lda #>ExplosionColour
    sta Player0ColourPtr+1

    ;; Set Gameover state (bit 1 in GameState, bit 3 for p1 won)
    lda GameState
    ora #%00000101
    sta GameState

    rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to Move Missles
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
MoveMissle subroutine
.MoveMissle0
    ;; Missle 0 goes up the screen
    lda Missle0YPos
    cmp #-1
    beq .MoveMissle1

    clc
    adc #2
    sta Missle0YPos
    cmp #191
    bcc .MoveMissle1

    lda #-1
    sta Missle0YPos

.MoveMissle1
    sta WSYNC

    ;; Missle 1 goes down the screen
    lda Missle1YPos
    cmp #-1
    beq .Finished

    sec
    sbc #2
    sta Missle1YPos
    clc
    cmp #0
    bcs .Finished

    lda #-1
    sta Missle1YPos

.Finished
    sta WSYNC
    rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to Check Fire buttons
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
CheckFireButtons subroutine
.CheckP0Fire:
    lda #%10000000
    bit INPT4
    bne .CheckP1Fire

    ;; If we are already firing then don't fire again
    lda Missle0YPos
    cmp #-1
    bne .CheckP1Fire

    ;; Start the PewPew animation and set the sounds
    lda #1
    sta PewPewP0
    sta AUDC0
    lda #AUDIO_VOLUME
    sta AUDV0

    ;; Lock the horizontal position of the missle
    lda #%00000010
    sta RESMP0
    lda #0
    sta RESMP0
    lda Player0XPos
    sta Missle0XPos

    ;; Start the missle animation
    lda Player0YPos
    clc
    adc PLAYER_HEIGHT+2
    sta Missle0YPos

.CheckP1Fire:
    lda #%10000000
    bit INPT5
    bne .Finished

    ;; If we are already firing then don't fire again
    lda Missle1YPos
    cmp #-1
    bne .Finished

    ;; Start the PewPew animation and set the sounds
    lda #1
    sta PewPewP1
    sta AUDC1
    lda #AUDIO_VOLUME
    sta AUDV1

    ;; Lock the horizontal position of the missle
    lda #%00000010
    sta RESMP1
    lda #0
    sta RESMP1
    lda Player1XPos
    sta Missle1XPos

    ;; Start the missle animation
    lda Player1YPos
    sta Missle1YPos

.Finished
    rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to Check For a missle hit on P0
;;
;; 18 when no collision
;; tons when collision
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
CheckForHits subroutine
.CheckForHitOnP0
    bit CXM1P            ; 3 cycles - bit 7 is Missle 1 collide with Player 0
    bpl .CheckForHitOnP1 ; 2/3

    jsr WinP1            ; tons

.CheckForHitOnP1
    bit CXM0P            ; 3 cycles - bit 7 is missle 0 collide with Player 1
    bpl .Finished        ; 2/3

    jsr WinP0            ; tons

.Finished
    rts                  ; 6

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to advance the frame of the explosion
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
AdvanceExplosionFrame subroutine
    ldy ExplosionFrame
    bcs .Finished

.ExplodeP0
    lda #%00000010
    bit GameState
    bne .ExplodeP1

    lda ExplosionLookupTableLowByte,Y
    sta Player0SpritePtr
    lda ExplosionLookupTableHighByte,Y
    sta Player0SpritePtr+1

.ExplodeP1
    lda #%00000100
    bit GameState
    bne .Continue

    lda ExplosionLookupTableLowByte,Y
    sta Player1SpritePtr
    lda ExplosionLookupTableHighByte,Y
    sta Player1SpritePtr+1

.Continue:
    lda ExplosionDelay
    clc
    adc #1
    and #7
    sta ExplosionDelay
    bne .Finished

    ;; We want to slow this animation 
    dey
    sty ExplosionFrame

.Finished
    rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Subroutine to set the scaline offsets from the Y Positions
;;
;;  26 cycles including rts
;;
;;                     -----------------------------------------
;;                  |  |                                       |
;;   ScanlinesToP1  |  |                                       |
;;                  |  |                                       |
;;                     |                 xxx                   |
;;         9           |                 xxx                   |
;;                     |                 xxx                   |
;;                  |  |                                       |
;;   ScanlinesToP0  |  |                                       |
;;                  |  |                                       |
;;                     |             xxx                       | 
;;         9           |             xxx                       | 
;;                     |             xxx                       | 
;;                  |  |                                       |
;;     Player0YPos  |  |                                       |
;;                  |  |                                       |
;;                     -----------------------------------------
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SetScanlineOffsets subroutine
    ;; The bottom of player 1 is 192 - p1_ypos, but we want the top so add
    ;; height to the total number of scanlines
    lda #192 - PLAYER_HEIGHT    ; 2
    sec                         ; 2
    sbc Player1YPos             ; 3
    sta ScanlinesToP1           ; 3

    lda Player1YPos             ; 3
    sec                         ; 2
    sbc Player0YPos             ; 3
    sbc PLAYER_HEIGHT           ; 2
    sta ScanlinesToP0           ; 3

    rts                         ; 6

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bitmaps
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

P0Sprite:
    .byte #%00000000    ;$00 ;
    .byte #%01000010    ;$40 ;  x    x 
    .byte #%01100110    ;$06 ;  xx  xx 
    .byte #%01111110    ;$06 ;  xxxxxx 
    .byte #%01011010    ;$06 ;  x xx x 
    .byte #%01011010    ;$06 ;  x xx x 
    .byte #%00011000    ;$06 ;    xx   
    .byte #%00111100    ;$94 ;   xxxx  
    .byte #%00011000    ;$1E ;    xx   
P0SpriteLeft:
    .byte #%00000000    ;$00 ;
    .byte #%01001000    ;$40 ; x  x  
    .byte #%01001000    ;$06 ; x  x  
    .byte #%01111000    ;$06 ; xxxx  
    .byte #%01111000    ;$06 ; xxxx  
    .byte #%01111000    ;$06 ; xxxx  
    .byte #%00110000    ;$06 ;  xx   
    .byte #%00110000    ;$94 ; xxxx  
    .byte #%00110000    ;$1E ;  xx   
P0SpriteRight:
    .byte #%00000000    ;$00 ;
    .byte #%00010010    ;$40 ;    x  x 
    .byte #%00010010    ;$06 ;    x  x 
    .byte #%00011110    ;$06 ;    xxxx 
    .byte #%00011110    ;$06 ;    xxxx 
    .byte #%00011110    ;$06 ;    xxxx 
    .byte #%00001100    ;$06 ;     xx  
    .byte #%00001100    ;$94 ;    xxxx 
    .byte #%00001100    ;$1E ;     xx  

P1Sprite:
    .byte #%00000000    ;$00 ;
    .byte #%00011000    ;$1E ;    xx   
    .byte #%00111100    ;$94 ;   xxxx  
    .byte #%00011000    ;$06 ;    xx   
    .byte #%01011010    ;$06 ;  x xx x 
    .byte #%01011010    ;$06 ;  x xx x 
    .byte #%01111110    ;$06 ;  xxxxxx 
    .byte #%01100110    ;$40 ;  xx  xx 
    .byte #%01000010    ;$06 ;  x    x 
P1SpriteLeft:
    .byte #%00000000    ;$00 ;
    .byte #%00110000    ;$06 ;  xx   
    .byte #%00110000    ;$94 ; xxxx  
    .byte #%00110000    ;$1E ;  xx   
    .byte #%01111000    ;$06 ; xxxx  
    .byte #%01111000    ;$06 ; xxxx  
    .byte #%01111000    ;$06 ; xxxx  
    .byte #%01001000    ;$40 ; x  x  
    .byte #%01001000    ;$06 ; x  x  
P1SpriteRight:
    .byte #%00000000    ;$00 ;
    .byte #%00001100    ;$06 ;     xx  
    .byte #%00001100    ;$94 ;    xxxx 
    .byte #%00001100    ;$1E ;     xx  
    .byte #%00011110    ;$06 ;    xxxx 
    .byte #%00011110    ;$06 ;    xxxx 
    .byte #%00011110    ;$06 ;    xxxx 
    .byte #%00010010    ;$40 ;    x  x 
    .byte #%00010010    ;$06 ;    x  x 

P0SpriteColour:
    .byte #$00    ; Black
    .byte #$40    ; Red
    .byte #$06    ; Light Gray 
    .byte #$06    ; Light Gray  
    .byte #$06    ; Light Gray 
    .byte #$06    ; Light Gray 
    .byte #$06    ; Light Gray
    .byte #$94    ; Light Blue
    .byte #$1E    ; Yellow

P1SpriteColour:
    .byte #$00    ; Black
    .byte #$DC    ; Green
    .byte #$64    ; Purple
    .byte #$06    ; Light Gray 
    .byte #$06    ; Light Gray  
    .byte #$06    ; Light Gray 
    .byte #$06    ; Light Gray 
    .byte #$06    ; Light Gray
    .byte #$40    ; Red


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Explosion - 7 frames
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ExplosionLookupTableLowByte:
    .byte #<ExplosionComplete
    .byte #<ExplosionFrame0
    .byte #<ExplosionFrame1
    .byte #<ExplosionFrame2
    .byte #<ExplosionFrame3
    .byte #<ExplosionFrame2
    .byte #<ExplosionFrame1
    .byte #<ExplosionFrame0

ExplosionLookupTableHighByte:
    .byte #>ExplosionComplete
    .byte #>ExplosionFrame0
    .byte #>ExplosionFrame1
    .byte #>ExplosionFrame2
    .byte #>ExplosionFrame3
    .byte #>ExplosionFrame2
    .byte #>ExplosionFrame1
    .byte #>ExplosionFrame0

ExplosionComplete:
    .byte #%00000000
    .byte #%00000000
    .byte #%00000000
    .byte #%00000000
    .byte #%00000000
    .byte #%00000000
    .byte #%00000000
    .byte #%00000000
    .byte #%00000000
ExplosionFrame0:
    .byte #%00000000  ;
    .byte #%00000000  ;
    .byte #%00000000  ;
    .byte #%00000000  ;
    .byte #%00011000  ;    xx
    .byte #%00011000  ;    xx
    .byte #%00000000  ;
    .byte #%00000000  ;
    .byte #%00000000  ;
ExplosionFrame1:
    .byte #%00000000  ;
    .byte #%00000000  ;
    .byte #%00000000  ;
    .byte #%00011000  ;    xx
    .byte #%00111100  ;   xxxx
    .byte #%00111100  ;   xxxx
    .byte #%00011000  ;    xx
    .byte #%00000000  ;
    .byte #%00000000  ;
ExplosionFrame2:
    .byte #%00000000  ;
    .byte #%00000000  ;
    .byte #%00011000  ;    xx
    .byte #%00111100  ;   xxxx
    .byte #%01111110  ;  xxxxxx
    .byte #%01111110  ;  xxxxxx
    .byte #%00111100  ;   xxxx
    .byte #%00011000  ;    xx
    .byte #%00000000  ;
ExplosionFrame3:
    .byte #%00000000  ;
    .byte #%00011000  ;    xx
    .byte #%00111100  ;   xxxx
    .byte #%01111110  ;  xxxxxx
    .byte #%11111111  ; xxxxxxxx
    .byte #%11111111  ; xxxxxxxx
    .byte #%01111110  ;  xxxxxx
    .byte #%00111100  ;   xxxx
    .byte #%00011000  ;    xx

ExplosionColour:
    .byte #$00; Black
    .byte #$30; Red
    .byte #$34; Dark Orange
    .byte #$38; Orange
    .byte #$1A; Yellow
    .byte #$1A; Yellow
    .byte #$38; Orange
    .byte #$34; Dark Orange
    .byte #$30; Red

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Pad to 4k and set reset and interrupt vectors
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    org $FFFC
    .word Start
    .word Start
