    processor 6502

    include "VCS.H"
    include "macro.h"

    seg code
    org $F000

START:
    CLEAN_START

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;                   228 colour clocks
;;    <------------------------------------------------>
;;
;;    |================================================|
;;    |            VERTICAL SYNC                       | 3 x VSYNC scanlines 
;;    |================================================|
;;    |                                                |
;;    |                                                |
;;    |            VERTICAL BLANK                      | 37 x VBLANK scanlines 
;;    |                                                |
;;    |                                                |
;;    |================================================|
;;    |            |                                   | 
;;    |            |                                   | 
;;    |   HORIZ.   |                                   | 
;;    |   BLANK    |                                   | 
;;    |            |                                   | 
;;    | <--------> | <-------------------------------> | 
;;    | 68 colour  |        160 colour clocks          | 
;;    |  clocks    |                                   | 
;;    |            |                                   | 
;;    |            |                                   | 
;;    |            |                                   | 
;;    |================================================|
;;    |                                                |
;;    |                                                | 30 x OVERSCAN scanlines
;;    |                                                |
;;    |================================================|
;;
;;    <------------------------------------------------>
;;                      76 CPU cycles
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


NextFrame:
    ;; Set register a to value 2
    lda #2
    ;; Store value of register a (#2) to the TIA VBLANK to enable the VBLANK
    sta VBLANK
    ;; Store value of register a (#2) to the TIA VSYNC memory address to enable VSYNC
    sta VSYNC

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; Generate the 3 lines of empty VSYNC
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    sta WSYNC   ; Set WSYNC to force the cpu to wait for the end of the 1st scanline
    sta WSYNC   ; Set WSYNC to force the cpu to wait for the end of the 2nd scanline
    sta WSYNC   ; Set WSYNC to force the cpu to wait for the end of the 3rd scanline

    lda #0
    sta VSYNC   ; We have now rendered the VSYNC

    ldx #37
LoopVBlank:
    sta WSYNC   ; Set WSYNC to force the cpu to wait for the end of the current VBLANK line
    dex
    bne LoopVBlank

    lda #0
    sta VBLANK  ; Q: Couldn't we just stx VBLANK instead, we know x is at 0 right now since we just looped with bne

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fill ROM to 4KB and set reset and interrupt vectors
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    org $FFFC
    .word START
    .word START
